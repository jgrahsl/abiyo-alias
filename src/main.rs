#![allow(dead_code)]
#![allow(unused_imports)]

// This is a poll driver that provides a push interface while honouring hysteresis of an averaged value

#[macro_use]
extern crate log;
extern crate pretty_env_logger;
extern crate structopt;

use std::cell::RefCell;
use std::collections::HashMap;
use std::io;
use std::io::prelude::*;
use std::io::prelude::*;
use std::path::PathBuf;
use std::process::exit;
use std::str;
use std::thread;
use std::time::{Duration, Instant};

use uuid::Uuid;
mod cmdline;
use cmdline::Opt;
use structopt::StructOpt;

use rumqtt::{MqttClient, MqttOptions, Notification, QoS};

// MQTT basics
const MQTT_SERVER_PORT: u16 = 1883;

fn main() {
    pretty_env_logger::init();
    let opt = Opt::from_args();

    info!("Serial device{:?}", &opt.device);

    let randomized = format!("{}-{}", &opt.mqtt_client_id, Uuid::new_v4());
    info!("Registering as client {}", randomized);

    let mqtt_options = MqttOptions::new(&randomized, &opt.mqtt_broker, MQTT_SERVER_PORT)
        .set_notification_channel_capacity(1000);
    let (mut mqtt_client, notifications) = MqttClient::start(mqtt_options).unwrap();

    mqtt_client
        .subscribe("abiyo/alias-define/#", QoS::AtLeastOnce)
        .unwrap();

    info!("Starting event loop");

    let mut data_container: HashMap<String, String> = HashMap::new();
    const DEFINE_TOPIC : &str = "abiyo/alias-define/";

    loop {
        let notification = notifications.recv_timeout(Duration::from_millis(1000));
        match notification {
            Ok(Notification::Publish(publish)) => {
                trace!("recved publish");

                if let Ok(pay) = str::from_utf8(&publish.payload) {
                    if let Some(arg) = publish.topic_name.strip_prefix(DEFINE_TOPIC) {
                        if let Ok(pay) = str::from_utf8(&publish.payload) {
                            println!("alias-define {} {}", arg, pay);

                            let topic = format!("{}", arg);
                            if !pay.is_empty() {
                                mqtt_client.subscribe(&topic, QoS::AtLeastOnce).unwrap();
                                data_container.insert(arg.to_owned(), pay.to_owned());
                                println!("subscribed to {}", topic);
                            } else {
                                println!("unsubscribed  {}", topic);
                                //mqtt_client.unsubscribe(topic).unwrap();
                                data_container.remove(arg);
                            }
                        }
                    } else {
                        if let Some(target) = data_container.get(&publish.topic_name) {
                            info!("forward {} {} {}", &publish.topic_name, target, pay);
                            mqtt_client
                                .publish(target, QoS::AtLeastOnce, true, pay)
                                .unwrap();
                        };
                    }
                }
            }
            Ok(Notification::Disconnection) => exit(0),
            Err(RecvTimeoutError) => {}
            _l => {
                println!("something else <{:?}>", _l);
                exit(0);
            }
        }
    }
}
